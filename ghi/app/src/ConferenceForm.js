import React, { useEffect, useState } from 'react';

function ConferenceForm(){
    const [locations, setLocations] = useState([]);

    const [name, setName] = useState([]);
    const [starts, setDateStart] = useState([]);
    const [ends, setDateEnd] = useState([]);
    const [description, setDescription] = useState([]);
    const [max_presentations, setMaxPresentations] = useState([]);
    const [max_attendees, setMaxAttendees] = useState([]);
    const [location, setLocation] = useState([]);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data ={}

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = max_presentations;
        data.max_attendees = max_attendees;
        data.location = location;

        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
            };
            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
            const newConference = await response.json();
            console.log(newConference)
            setName('');
            setDateStart('');
            setDateEnd('');
            setDescription('');
            setLocation('');
            setMaxPresentations('');
            setMaxAttendees('');

            }

    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleDateStartChange = (event) =>{
        const value = event.target.value;
        setDateStart(value);
    }
    const handleDateEndChange = (event) =>{
        const value = event.target.value;
        setDateEnd(value);
    }
    const handleDescriptionChange = (event) =>{
        const value = event.target.value;
        setDescription(value);
    }
    const handleMaxPresentationsChange = (event) =>{
        const value = event.target.value;
        setMaxPresentations(value);
    }
    const handleMaxAttendeesChange = (event) =>{
        const value = event.target.value;
        setMaxAttendees(value);
    }
    const handleLocationChange = (event) =>{
        const value = event.target.value;
        setLocation(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();
            setLocations(data.locations)
            console.log(data.locations)

        }

        console.log(locations)
    }
useEffect(() => {
    fetchData();
    }, []);



    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={starts} onChange={handleDateStartChange} placeholder="Date-start" required type="date" name="starts" id="date-start" className="form-control"/>
                <label htmlFor="Date-start">Date Start</label>
              </div>
              <div className="form-floating mb-3">
                <input value={ends} onChange={handleDateEndChange} placeholder="Date-end"  required type="date" name="ends" id="date-end" className="form-control"/>
                <label htmlFor="Date-end">Date Ends</label>
              </div>
              <div className="form-floating mb-3">
                <label htmlFor="Description" className="form-label">Description</label>
                <textarea value={description} onChange={handleDescriptionChange} name="description" className="form-control" id="description" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
              <input value={max_presentations} onChange={handleMaxPresentationsChange} placeholder="Max-presentations" required type="number" id="max-presentations" name="max_presentations" className="form-control"/>
              <label htmlFor="Max-presentations">Max Presentations</label>
            </div>
            <div className="form-floating mb-3">
            <input value={max_attendees} onChange={handleMaxAttendeesChange} placeholder="Max-attendees" required type="number" id="max-attendees" name="max_attendees" className="form-control"/>
            <label htmlFor="Max-attendees">Max Attendees</label>
          </div>
            <div className="mb-3">
          <select onChange={handleLocationChange} required id="location" name="location" className="form-select">
          <option value="">Choose a location</option>
          {locations.map(location1 =>{
            return (
                <option key={location1.id} value={location1.id}>
                    {location1.name}
                </option>
            )
          })}
          </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
              </div>

            </div>
            </div>
    );
}

export default ConferenceForm;
