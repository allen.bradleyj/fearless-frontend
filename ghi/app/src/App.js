import Nav from './Nav'
import AttendeesList from './AttendeesList';
import MainPage from './MainPage'
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm'
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import { BrowserRouter, Outlet } from "react-router-dom";
import { Routes, Route } from 'react-router-dom';

export default function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (

    <BrowserRouter>
    <Nav />
    <div className="container">
      <Routes>
        <Route path="mainpage">
          <Route path ="" element={<MainPage />} />
        </Route>
        <Route path="locations">
          <Route path ="new" element={<LocationForm />} />
        </Route>
        <Route path="conferences">
          <Route path ="new" element={<ConferenceForm />} />
        </Route>

        <Route path="attendees">
          <Route path ="" element={<AttendeesList attendees= {props.attendees}/>} />
          <Route path ="new" element={<AttendConferenceForm />} />
        </Route>

          <Route path="presentation">
          <Route path ="" element={<PresentationForm/>} />
        </Route>

      </Routes>
    </div>

    </BrowserRouter>

  );
}
