import React, {useEffect, useState } from 'react';

function PresentationForm(){
    const [conference, setConference] = useState([]);
    const [name, setName] = useState([]);
    const [email, setEmail] = useState([]);
    const [conferences, setConferences] = useState([]);
    const [company_name, setCompany] = useState([]);
    const [title, setTitle] = useState([]);
    const [synopsis, setSynopsis] = useState([]);

    const fetchData = async () => {
      const url = 'http://localhost:8000/api/conferences/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
      }
    }

    useEffect(() => {
      fetchData();
    }, []);

    const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {};
      data.conference = conference;
      data.presenter_name = name;
      data.presenter_email = email;
      data.title = title;
      data.company_name = company_name;
      data.synopsis = synopsis;

      console.log(data)

      const url = `http://localhost:8000/api/conferences/${data.conference}/presentations/`;
      const fetchOptions = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(url, fetchOptions);
      if (response.ok) {
        const newPresentation = await response.json();
        console.log(newPresentation)

        setConference('');
        setName('');
        setEmail('');
      }
    }

    const handleChangeConference = (event) => {
      const value = event.target.value;
      setConference(value);
    }

    const handleChangeName = (event) => {
      const value = event.target.value;
      setName(value);
    }

    const handleChangeEmail = (event) => {
      const value = event.target.value;
      setEmail(value);
    }
    const handleChangeCompany = (event) => {
        const value = event.target.value;
        setCompany(value);
      }

    const handleChangeTitle = (event) => {
        const value = event.target.value;
        setTitle(value);
    }
    const handleChangeSynopsis = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }





    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new presentation</h1>
              <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                  <input onChange={handleChangeName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                  <label htmlFor="presenter_name">Presenter name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleChangeEmail} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                  <label htmlFor="presenter_email">Presenter email</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleChangeCompany} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                  <label htmlFor="company_name">Company name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleChangeTitle} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                  <label htmlFor="title">Title</label>
                </div>
                <div className="mb-3">
                  <label onChange={handleChangeSynopsis} htmlFor="synopsis">Synopsis</label>
                  <textarea className="form-control" id="synopsis" rows="3" name="synopsis" className="form-control"/>
                </div>
                <div className="mb-3">
                  <select value = {conference} onChange={handleChangeConference} name="conference" id="conference" className="form-select">
                    <option value="">Choose a conference</option>
                    {conferences.map(conference =>{
                    return (

                    <option key={conference.id} value={conference.id}>
                            {conference.name}
                        </option>
                        )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }

export default PresentationForm;
