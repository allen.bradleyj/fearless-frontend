import { NavLink } from "react-router-dom";

function Nav() {
  return (
<nav className="navbar navbar-expand-lg bg-body-tertiary">
  <div className="container-fluid">
  <NavLink className="navbar-brand" to="/mainpage">
    Conference GO!
    </NavLink>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarNav">
      <ul className="navbar-nav">
        <li className="nav-item">
          <NavLink className="nav-link active" aria-current="page" to="/locations/new">
            New location
            </NavLink>
        </li>
        <li className="nav-item">
        <NavLink className="nav-link" aria-current="page" to="/attendees">
          Attendees
        </NavLink>
        </li>
        <li className="nav-item">
        <NavLink className="nav-link" aria-current="page" to="/attendees/new">
          New Attendee
        </NavLink>
        </li>
        <li className="nav-item">
        <NavLink className="nav-link" aria-current="page" to="/conferences/new">
          New Conference
        </NavLink>
        </li>
        <li>
        <NavLink className="nav-link" aria-current="page" to="/presentation">
          Presentation
        </NavLink>
        </li>

      </ul>
    </div>
  </div>
</nav>
);
  }

  export default Nav;
