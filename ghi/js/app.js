function createCard(name, description, pictureUrl) {
    return `
      <div class="card" style="box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.792); margin-bottom: 20px; max-width:400px;">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
        </div>
      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    let colSelect = 1
    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {

            const details = await detailResponse.json();
            console.log(details)
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(title, description, pictureUrl);
            console.log(colSelect)
            const column = document.querySelector(`.col${colSelect}`);
            column.innerHTML += html;
            colSelect += 1
            if (colSelect === 4){colSelect = 1}


          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
    }

  });
